// Variable global para mantener el top score
var topScore = 0;

/**
 * Game Scene class
 */
const SCENE_WIDTH = 256;
const SCENE_HEIGHT = 240;
const CAMERA_MOTION_POINT = 96; 
const CAMERA_VELOCITY = 2;
const BLOCK_SIZE = 16;
const ENEMY_VELOCITY = -40;
const LEVEL_TIME = 400;
const END_GAME_DELAY = 3000;

class Game extends Phaser.Scene {

    init(lastGame) {
        this.levelEnds = false;

        // Scoring
        let lastScore = !lastGame.score ? 0 : lastGame.score;
        topScore = lastScore > topScore ? lastScore : topScore;
        this.score = 0;        
    }

    preload() {
        // Fonts
        this.load.bitmapFont('font-white', 'assets/fonts/font-white.png', 'assets/fonts/font.fnt');
        this.load.bitmapFont('font-pink', 'assets/fonts/font-pink.png', 'assets/fonts/font.fnt');

        this.load.image('player-mushroom', 'assets/images/player-mushroom.png');
        this.load.image('banner', 'assets/images/banner.png');
        this.load.image('score-coin', 'assets/images/score-coin-shadow.png');
        this.load.image('bg-1-1a', 'assets/images/levels/bg-1-1a.png');        
        this.load.image('bg-1-1b', 'assets/images/levels/bg-1-1b.png');
        
        // Mario
        this.load.spritesheet('mario-small', 'assets/sprites/mario-small.png', { frameWidth: 16, frameHeight: 16 });
        this.load.spritesheet('mario-big', 'assets/sprites/mario-big.png', { frameWidth: 16, frameHeight: 32 });
        this.load.spritesheet('mario-sizes', 'assets/sprites/mario-sizes.png', { frameWidth: 16, frameHeight: 32 });

        // Enemies
        this.load.spritesheet('grump', 'assets/sprites/grump.png', { frameWidth: 16, frameHeight: 16 });
        this.load.spritesheet('koopa', 'assets/sprites/koopa.png', { frameWidth: 16, frameHeight: 24 });

        // Points
        this.load.image('100', 'assets/images/points/100.png');
        this.load.image('200', 'assets/images/points/200.png');
        this.load.image('400', 'assets/images/points/400.png');
        this.load.image('500', 'assets/images/points/500.png');
        this.load.image('800', 'assets/images/points/800.png');
        this.load.image('1000', 'assets/images/points/1000.png');
        this.load.image('2000', 'assets/images/points/2000.png');
        this.load.image('4000', 'assets/images/points/4000.png');
        this.load.image('5000', 'assets/images/points/5000.png');
        this.load.image('8000', 'assets/images/points/8000.png');
        this.load.image('1UP', 'assets/images/points/1UP.png');

        // Blocks
        this.load.image('red-m', 'assets/images/blocks/red-mushroom.png');
        this.load.image('green-m', 'assets/images/blocks/green-mushroom.png');
        this.load.image('ground-block', 'assets/images/blocks/ground-block.png');
        this.load.image('brick-block', 'assets/images/blocks/brick-block.png');
        this.load.image('stair-block', 'assets/images/blocks/stair-block.png');
        this.load.image('armored-block', 'assets/images/blocks/armored-block.png');        
        this.load.spritesheet('question-block', 'assets/sprites/question-block.png', { frameWidth: 16, frameHeight: 16 });
        this.load.spritesheet('coin', 'assets/sprites/coin.png', { frameWidth: 8, frameHeight: 16 });
        this.load.image('s-pipe', 'assets/images/blocks/small-pipe.png');
        this.load.image('m-pipe', 'assets/images/blocks/medium-pipe.png');
        this.load.image('h-pipe', 'assets/images/blocks/high-pipe.png');
        this.load.image('final-flag', 'assets/images/blocks/final-flag.png');
        this.load.image('castle-flag', 'assets/images/blocks/castle-flag.png');
        this.load.image('castle', 'assets/images/blocks/castle.png');

        // Audio
        this.load.audio('jump-small', 'assets/audios/jump-small.mp3');
        this.load.audio('jump-big', 'assets/audios/jump-big.mp3');
        this.load.audio('power-up', 'assets/audios/power-up.mp3');
        this.load.audio('coin-sound', 'assets/audios/coin.mp3');
        this.load.audio('mushroom-sound', 'assets/audios/mushroom.mp3');
        this.load.audio('stomp-sound', 'assets/audios/stomp.mp3');
        this.load.audio('die-sound', 'assets/audios/die.mp3');
        this.load.audio('stage-clear', 'assets/audios/stage-clear.mp3');    
        this.load.audio('flag-pole', 'assets/audios/flag-pole.mp3');
        this.load.audio('music', 'assets/audios/music.mp3');
    }

    create() {
        // Level background
        this.background = this.add.image(0, 0, 'bg-1-1a').setDepth(0).setOrigin(0);

        // GameObject Map (blocks, enemies, etc.)
        this.goMap = {
            // Blocks
            brickBlocks: [ {c:20, r:9}, {c:22, r:9}, {c:24, r:9}, {c:77, r:9}, {c:79, r:9}, {c:80, r:5}, {c:81, r:5}, {c:82, r:5}, {c:83, r:5}, {c:84, r:5}, {c:85, r:5}, {c:86, r:5}, {c:87, r:5}, {c:91, r:5}, {c:92, r:5}, {c:93, r:5}, {c:94, r:9}, {c:100, r:9}, {c:101, r:9}, {c:118, r:9}, {c:121, r:5}, {c:122, r:5}, {c:123, r:5}, {c:128, r:5}, {c:129, r:9}, {c:130, r:9}, {c:131, r:5}, {c:168, r:9}, {c:169, r:9}, {c:171, r:9} ],
            questionBlocks: [ {c:16, r:9, t:'coin', p:'200'}, {c:21, r:9, t:'red-m', p:'200'}, {c:22, r:5, t:'coin', p:'200'}, {c:23, r:9, t:'coin', p:'200'}, {c:78, r:9, t:'coin', p:'200'}, {c:94, r:5, t:'coin', p:'200'}, {c:106, r:9, t:'coin', p:'200'}, {c:109, r:9, t:'coin', p:'200'}, {c:109, r:5, t:'coin', p:'200'}, {c:112, r:9, t:'coin', p:'200'}, {c:129, r:5, t:'coin', p:'200'}, {c:130, r:5, t:'coin', p:'200'}, {c:170, r:9, t:'coin', p:'200'} ],
            groundBlocks: [ {c:0, r:13}, {c:0, r:14}, {c:1, r:13}, {c:1, r:14}, {c:2, r:13}, {c:2, r:14}, {c:3, r:13}, {c:3, r:14}, {c:4, r:13}, {c:4, r:14}, {c:5, r:13}, {c:5, r:14}, {c:6, r:13}, {c:6, r:14}, {c:7, r:13}, {c:7, r:14}, {c:8, r:13}, {c:8, r:14}, {c:9, r:13}, {c:9, r:14}, {c:10, r:13}, {c:10, r:14}, {c:11, r:13}, {c:11, r:14}, {c:12, r:13}, {c:12, r:14}, {c:13, r:13}, {c:13, r:14}, {c:14, r:13}, {c:14, r:14}, {c:15, r:13}, {c:15, r:14}, {c:16, r:13}, {c:16, r:14}, {c:17, r:13}, {c:17, r:14}, {c:18, r:13}, {c:18, r:14}, {c:19, r:13}, {c:19, r:14}, {c:20, r:13}, {c:20, r:14}, {c:21, r:13}, {c:21, r:14}, {c:22, r:13}, {c:22, r:14}, {c:23, r:13}, {c:23, r:14}, {c:24, r:13}, {c:24, r:14}, {c:25, r:13}, {c:25, r:14}, {c:26, r:13}, {c:26, r:14}, {c:27, r:13}, {c:27, r:14}, {c:28, r:13}, {c:28, r:14}, {c:29, r:13}, {c:29, r:14}, {c:30, r:13}, {c:30, r:14}, {c:31, r:13}, {c:31, r:14}, {c:32, r:13}, {c:32, r:14}, {c:33, r:13}, {c:33, r:14}, {c:34, r:13}, {c:34, r:14}, {c:35, r:13}, {c:35, r:14}, {c:36, r:13}, {c:36, r:14}, {c:37, r:13}, {c:37, r:14}, {c:38, r:13}, {c:38, r:14}, {c:39, r:13}, {c:39, r:14}, {c:40, r:13}, {c:40, r:14}, {c:41, r:13}, {c:41, r:14}, {c:42, r:13}, {c:42, r:14}, {c:43, r:13}, {c:43, r:14}, {c:44, r:13}, {c:44, r:14}, {c:45, r:13}, {c:45, r:14}, {c:46, r:13}, {c:46, r:14}, {c:47, r:13}, {c:47, r:14}, {c:48, r:13}, {c:48, r:14}, {c:49, r:13}, {c:49, r:14}, {c:50, r:13}, {c:50, r:14}, {c:51, r:13}, {c:51, r:14}, {c:52, r:13}, {c:52, r:14}, {c:53, r:13}, {c:53, r:14}, {c:54, r:13}, {c:54, r:14}, {c:55, r:13}, {c:55, r:14}, {c:56, r:13}, {c:56, r:14}, {c:57, r:13}, {c:57, r:14}, {c:58, r:13}, {c:58, r:14}, {c:59, r:13}, {c:59, r:14}, {c:60, r:13}, {c:60, r:14}, {c:61, r:13}, {c:61, r:14}, {c:62, r:13}, {c:62, r:14}, {c:63, r:13}, {c:63, r:14}, {c:64, r:13}, {c:64, r:14}, {c:65, r:13}, {c:65, r:14}, {c:66, r:13}, {c:66, r:14}, {c:67, r:13}, {c:67, r:14}, {c:68, r:13}, {c:68, r:14}, {c:71, r:13}, {c:71, r:14}, {c:72, r:13}, {c:72, r:14}, {c:73, r:13}, {c:73, r:14}, {c:74, r:13}, {c:74, r:14}, {c:75, r:13}, {c:75, r:14}, {c:76, r:13}, {c:76, r:14}, {c:77, r:13}, {c:77, r:14}, {c:78, r:13}, {c:78, r:14}, {c:79, r:13}, {c:79, r:14}, {c:80, r:13}, {c:80, r:14}, {c:81, r:13}, {c:81, r:14}, {c:82, r:13}, {c:82, r:14}, {c:83, r:13}, {c:83, r:14}, {c:84, r:13}, {c:84, r:14}, {c:85, r:13}, {c:85, r:14}, {c:89, r:13}, {c:89, r:14}, {c:90, r:13}, {c:90, r:14}, {c:91, r:13}, {c:91, r:14}, {c:92, r:13}, {c:92, r:14}, {c:93, r:13}, {c:93, r:14}, {c:94, r:13}, {c:94, r:14}, {c:95, r:13}, {c:95, r:14}, {c:96, r:13}, {c:96, r:14}, {c:97, r:13}, {c:97, r:14}, {c:98, r:13}, {c:98, r:14}, {c:99, r:13}, {c:99, r:14}, {c:100, r:13}, {c:100, r:14}, {c:101, r:13}, {c:101, r:14}, {c:102, r:13}, {c:102, r:14}, {c:103, r:13}, {c:103, r:14}, {c:104, r:13}, {c:104, r:14}, {c:105, r:13}, {c:105, r:14}, {c:106, r:13}, {c:106, r:14}, {c:107, r:13}, {c:107, r:14}, {c:108, r:13}, {c:108, r:14}, {c:109, r:13}, {c:109, r:14}, {c:110, r:13}, {c:110, r:14}, {c:111, r:13}, {c:111, r:14}, {c:112, r:13}, {c:112, r:14}, {c:113, r:13}, {c:113, r:14}, {c:114, r:13}, {c:114, r:14}, {c:115, r:13}, {c:115, r:14}, {c:116, r:13}, {c:116, r:14}, {c:117, r:13}, {c:117, r:14}, {c:118, r:13}, {c:118, r:14}, {c:119, r:13}, {c:119, r:14}, {c:120, r:13}, {c:120, r:14}, {c:121, r:13}, {c:121, r:14}, {c:122, r:13}, {c:122, r:14}, {c:123, r:13}, {c:123, r:14}, {c:124, r:13}, {c:124, r:14}, {c:125, r:13}, {c:125, r:14}, {c:126, r:13}, {c:126, r:14}, {c:127, r:13}, {c:127, r:14}, {c:128, r:13}, {c:128, r:14}, {c:129, r:13}, {c:129, r:14}, {c:130, r:13}, {c:130, r:14}, {c:131, r:13}, {c:131, r:14}, {c:132, r:13}, {c:132, r:14}, {c:133, r:13}, {c:133, r:14}, {c:134, r:13}, {c:134, r:14}, {c:135, r:13}, {c:135, r:14}, {c:136, r:13}, {c:136, r:14}, {c:137, r:13}, {c:137, r:14}, {c:138, r:13}, {c:138, r:14}, {c:139, r:13}, {c:139, r:14}, {c:140, r:13}, {c:140, r:14}, {c:141, r:13}, {c:141, r:14}, {c:142, r:13}, {c:142, r:14}, {c:143, r:13}, {c:143, r:14}, {c:144, r:13}, {c:144, r:14}, {c:145, r:13}, {c:145, r:14}, {c:146, r:13}, {c:146, r:14}, {c:147, r:13}, {c:147, r:14}, {c:148, r:13}, {c:148, r:14}, {c:149, r:13}, {c:149, r:14}, {c:150, r:13}, {c:150, r:14}, {c:151, r:13}, {c:151, r:14}, {c:152, r:13}, {c:152, r:14}, {c:155, r:13}, {c:155, r:14}, {c:156, r:13}, {c:156, r:14}, {c:157, r:13}, {c:157, r:14}, {c:158, r:13}, {c:158, r:14}, {c:159, r:13}, {c:159, r:14}, {c:160, r:13}, {c:160, r:14}, {c:161, r:13}, {c:161, r:14}, {c:162, r:13}, {c:162, r:14}, {c:163, r:13}, {c:163, r:14}, {c:164, r:13}, {c:164, r:14}, {c:165, r:13}, {c:165, r:14}, {c:166, r:13}, {c:166, r:14}, {c:167, r:13}, {c:167, r:14}, {c:168, r:13}, {c:168, r:14}, {c:169, r:13}, {c:169, r:14}, {c:170, r:13}, {c:170, r:14}, {c:171, r:13}, {c:171, r:14}, {c:172, r:13}, {c:172, r:14}, {c:173, r:13}, {c:173, r:14}, {c:174, r:13}, {c:174, r:14}, {c:175, r:13}, {c:175, r:14}, {c:176, r:13}, {c:176, r:14}, {c:177, r:13}, {c:177, r:14}, {c:178, r:13}, {c:178, r:14}, {c:179, r:13}, {c:179, r:14}, {c:180, r:13}, {c:180, r:14}, {c:181, r:13}, {c:181, r:14}, {c:182, r:13}, {c:182, r:14}, {c:183, r:13}, {c:183, r:14}, {c:184, r:13}, {c:184, r:14}, {c:185, r:13}, {c:185, r:14}, {c:186, r:13}, {c:186, r:14}, {c:187, r:13}, {c:187, r:14}, {c:188, r:13}, {c:188, r:14}, {c:189, r:13}, {c:189, r:14}, {c:190, r:13}, {c:190, r:14}, {c:191, r:13}, {c:191, r:14}, {c:192, r:13}, {c:192, r:14}, {c:193, r:13}, {c:193, r:14}, {c:194, r:13}, {c:194, r:14}, {c:195, r:13}, {c:195, r:14}, {c:196, r:13}, {c:196, r:14}, {c:197, r:13}, {c:197, r:14}, {c:198, r:13}, {c:198, r:14}, {c:199, r:13}, {c:199, r:14}, {c:200, r:13}, {c:200, r:14}, {c:201, r:13}, {c:201, r:14}, {c:202, r:13}, {c:202, r:14}, {c:203, r:13}, {c:203, r:14}, {c:204, r:13}, {c:204, r:14}, {c:205, r:13}, {c:205, r:14}, {c:206, r:13}, {c:206, r:14}, {c:207, r:13}, {c:207, r:14}, {c:208, r:13}, {c:208, r:14}, {c:209, r:13}, {c:209, r:14}, {c:210, r:13}, {c:210, r:14} ],
            pipeTubes: [ {c:28, r:11, t:'s-pipe'}, {c:38, r:10, t:'m-pipe'}, {c:46, r:9, t:'h-pipe'}, {c:57, r:9, t:'h-pipe'}, {c:163, r:11, t:'s-pipe'}, {c:179, r:11, t:'s-pipe'} ],
            stairBlocks: [ {c:134, r:12}, {c:135, r:12}, {c:135, r:11}, {c:136, r:12}, {c:136, r:11}, {c:136, r:10}, {c:137, r:12}, {c:137, r:11}, {c:137, r:10}, {c:137, r:9}, {c:140, r:12}, {c:140, r:11}, {c:140, r:10}, {c:140, r:9}, {c:141, r:12}, {c:141, r:11}, {c:141, r:10}, {c:142, r:12}, {c:142, r:11}, {c:143, r:12}, {c:148, r:12}, {c:149, r:12}, {c:149, r:11}, {c:150, r:12}, {c:150, r:11}, {c:150, r:10}, {c:151, r:12}, {c:151, r:11}, {c:151, r:10}, {c:151, r:9}, {c:152, r:12}, {c:152, r:11}, {c:152, r:10}, {c:152, r:9}, {c:155, r:12}, {c:155, r:11}, {c:155, r:10}, {c:155, r:9}, {c:156, r:12}, {c:156, r:11}, {c:156, r:10}, {c:157, r:12}, {c:157, r:11}, {c:158, r:12}, {c:181, r:12}, {c:182, r:12}, {c:182, r:11}, {c:183, r:12}, {c:183, r:11}, {c:183, r:10}, {c:184, r:12}, {c:184, r:11}, {c:184, r:10}, {c:184, r:9}, {c:185, r:12}, {c:185, r:11}, {c:185, r:10}, {c:185, r:9}, {c:185, r:8}, {c:186, r:12}, {c:186, r:11}, {c:186, r:10}, {c:186, r:9}, {c:186, r:8}, {c:186, r:7}, {c:187, r:12}, {c:187, r:11}, {c:187, r:10}, {c:187, r:9}, {c:187, r:8}, {c:187, r:7}, {c:187, r:6}, {c:188, r:12}, {c:188, r:11}, {c:188, r:10}, {c:188, r:9}, {c:188, r:8}, {c:188, r:7}, {c:188, r:6}, {c:188, r:5}, {c:189, r:12}, {c:189, r:11}, {c:189, r:10}, {c:189, r:9}, {c:189, r:8}, {c:189, r:7}, {c:189, r:6}, {c:189, r:5}, {c:198, r:12} ],
            hiddenBlocks: [ {c:64, r:8} ], // TODO
            // Final flag & castle
            finalFlag: {c:197, r:3},
            castle: {c:202, r:8},
            castleFlag: {c:204, r:8},
            // Enemies
            grumps: [ {c:24, r:12}, {c:40, r:12}, {c:50, r:12}, {c:52, r:12}, {c:81, r:4}, {c:83, r:4}, {c:103, r:12}, {c:105, r:12}, {c:121, r:12}, {c:123, r:12}, {c:127, r:12}, {c:129, r:12}, {c:132, r:12}, {c:133, r:12}, {c:175, r:12}, {c:176, r:12} ],
            koopas: [ {c:112, r:12} ],
        };

        this.blocks = this.physics.add.staticGroup();

        // Ground Blocks
        for (let block of this.goMap.groundBlocks) {
            this.blocks.create(block.c * BLOCK_SIZE, block.r * BLOCK_SIZE, 'ground-block').setDepth(1).setOrigin(0).refreshBody();
        }

        // Brick Blocks
        for (let block of this.goMap.brickBlocks) {
            this.blocks.create(block.c * BLOCK_SIZE, block.r * BLOCK_SIZE, 'brick-block').setDepth(1).setOrigin(0).refreshBody();
        }

        // Question Blocks
        this.questionBlocks = this.physics.add.staticGroup();   
        this.anims.create({
            key: 'question-block-anim',
            frames: this.anims.generateFrameNumbers('question-block', { frames: [ 0, 1, 2, 1, 0 ] }),
            frameRate: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'coin-anim',
            frames: this.anims.generateFrameNumbers('coin', { frames: [ 2, 3, 0, 1 ] }),
            frameRate: 20,
            repeat: 4
        });
        
        for (let block of this.goMap.questionBlocks) {
            let sprite = this.questionBlocks.create(block.c * BLOCK_SIZE, block.r * BLOCK_SIZE, 'question-block').setDepth(1).setOrigin(0).refreshBody();
            sprite.t = block.t; // Texture
            sprite.p = block.p; // Points
            sprite.body.x += 1;
            
            // Truco para poder pegarle cuando el bloque está rodeado por otros bloques
            sprite.body.width = 14;
            sprite.body.height = 17;

            sprite.play('question-block-anim', true);
        }

        // Pipe Tubes
        for (let block of this.goMap.pipeTubes) {
            this.blocks.create(block.c * BLOCK_SIZE, block.r * BLOCK_SIZE, block.t).setDepth(1).setOrigin(0).refreshBody();
        }

        // Stair Blocks
        for (let i=0; i<this.goMap.stairBlocks.length; i++) {
            let block = this.goMap.stairBlocks[i];
            this.blocks.create(block.c * BLOCK_SIZE, block.r * BLOCK_SIZE, 'stair-block').setDepth(1).setOrigin(0).refreshBody();
        }

        // Hidden Blocks
        for (let block of this.goMap.hiddenBlocks) {
            this.blocks.create(block.c * BLOCK_SIZE, block.r * BLOCK_SIZE, 'armored-block').setDepth(1).setOrigin(0).refreshBody().setVisible(false); // TODO CAMBIAR ESTE BLOQUE Y HACERLO INVISIBLE !!
        }

        // Final Flag
        this.finalFlag = this.blocks.create(this.goMap.finalFlag.c * BLOCK_SIZE, this.goMap.finalFlag.r * BLOCK_SIZE, 'final-flag').setDepth(1).setOrigin(0).refreshBody();

        // Castle
        this.castle = this.add.image(this.goMap.castle.c * BLOCK_SIZE, this.goMap.castle.r * BLOCK_SIZE, 'castle').setDepth(1).setOrigin(0);

        // Castle Flag
        this.castleFlag = this.add.image(this.goMap.castleFlag.c * BLOCK_SIZE, this.goMap.castleFlag.r * BLOCK_SIZE, 'castle-flag').setDepth(0).setOrigin(0);

        // Enemies
        this.enemies = this.physics.add.group({ bounceX: 1 });

        // Grumps
        this.anims.create({
            key: 'grump-walk-anim',
            frames: this.anims.generateFrameNumbers('grump', { frames: [ 0, 1 ] }),
            frameRate: 5,
            repeat: -1
        });
        
        for (let enemy of this.goMap.grumps) {
            let sprite = this.enemies.create(enemy.c * BLOCK_SIZE, enemy.r * BLOCK_SIZE, 'grump').setDepth(1).setOrigin(0).refreshBody();
            sprite.play('grump-walk-anim', true);
        }

        // Koopas
        this.anims.create({
            key: 'koopa-walk-left-anim',
            frames: this.anims.generateFrameNumbers('koopa', { frames: [ 0, 1 ] }),
            frameRate: 5,
            repeat: -1
        });

        for (let enemy of this.goMap.koopas) {
            let sprite = this.enemies.create(enemy.c * BLOCK_SIZE, enemy.r * BLOCK_SIZE - 8, 'koopa').setDepth(1).setOrigin(0).refreshBody();
            sprite.play('koopa-walk-left-anim', true);
        }
        
        // Premios (mushroom, flower, etc.)
        this.prizes = this.physics.add.group({ bounceX: 1 });

        // Mario
        this.mario = new Mario(this);

        this.anims.create({
            key: 'mario-small-runs-anim',
            frames: this.anims.generateFrameNumbers('mario-small', { frames: [ 1, 2, 3 ] }), // {start: 1, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'mario-big-runs-anim',
            frames: this.anims.generateFrameNumbers('mario-big', { frames: [ 1, 2, 3 ] }), // {start: 1, end: 3 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'mario-grows-anim',
            frames: this.anims.generateFrameNumbers('mario-sizes', { frames: [ 0, 1, 0, 1, 0, 1, 2, 0, 1, 2 ] }),
            frameRate: 10,
            repeat: 0
        });

        this.anims.create({
            key: 'mario-shrinks-anim',
            frames: this.anims.generateFrameNumbers('mario-sizes', { frames: [ 2, 1, 2, 1, 2, 1, 0, 2, 1, 0 ] }),
            frameRate: 10,
            repeat: 0
        });

        this.anims.create({
            key: 'mario-small-slide-down',
            frames: this.anims.generateFrameNumbers('mario-small', { frames: [ 6, 7 ] }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'mario-big-slide-down',
            frames: this.anims.generateFrameNumbers('mario-big', { frames: [ 6, 7 ] }),
            frameRate: 10,
            repeat: -1
        });

        // Start screen
        this.add.bitmapText(24, 8, 'font-white', 'MARIO', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.scoreText = this.add.bitmapText(24, 16, 'font-white', '000000', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.add.bitmapText(144, 8, 'font-white', 'WORLD', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.add.bitmapText(152, 16, 'font-white', '1-1', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.add.bitmapText(200, 8, 'font-white', 'TIME', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.timeText = this.add.bitmapText(208, 16, 'font-white', '400', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.add.bitmapText(97, 16, 'font-white', 'x00', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.add.image(89, 16, 'score-coin').setOrigin(0).setScrollFactor(0);

        // Menu options
        this.add.image(40, 32, 'banner').setDepth(1).setOrigin(0);
        this.add.bitmapText(104, 120, 'font-pink', '@1985 NINTENDO', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);
        this.add.bitmapText(88, 144, 'font-white', '1 PLAYER GAME', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);
        this.add.bitmapText(88, 160, 'font-white', '2 PLAYER GAME', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);
        this.add.image(72, 144, 'player-mushroom').setOrigin(0);
        let topScoreText = (`000000${topScore}`).slice(-6);
        this.add.bitmapText(96, 184, 'font-white', `TOP- ${topScoreText}`, 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);

        // Timer
        this.counter = LEVEL_TIME;
        this.timer = this.time.addEvent({
            delay: 1000, // 1 second
            loop: true,
            callback: () => {
                this.setTime();
                this.counter--;        
                if (this.counter == 0) {     
                    this.setTime();
                    this.mario.die();
                    this.timer.remove();
                }
            }
        });
        
        // Keyboard
        this.input.keyboard.enabled = true;
        this.cursors = this.input.keyboard.createCursorKeys();

        // Collisions & Overlaps
        // Bloques
        this.physics.add.collider(this.mario, this.blocks);
        this.physics.add.collider(this.mario, this.prizes, (mario, prize) => {
            if (mario.visible) {
                prize.destroy();
                this.mario.grow();
            }
        });

        this.physics.add.overlap(this.mario, this.enemies, (mario, enemy) => {
            if (!this.mario.isProtected() && this.mario.isAlive() && !enemy.die) {
                if (!enemy.body.touching.up) {
                    this.mario.hurt();
                } else {
                    this.mario.hurtEnemy(enemy);
                }
            }
        });

        this.physics.add.collider(this.prizes, this.blocks);
        this.physics.add.collider(this.enemies, this.blocks);

        // Question Blocks
        this.physics.add.collider(this.mario, this.questionBlocks, (mario, block) => {
            if (!block.immovable && block.body.touching.down) {
                switch(block.t) {
                case 'coin':
                    let coin = this.add.sprite(block.x + 4, block.y - 16, block.t).setDepth(1).setOrigin(0);
                    coin.play('coin-anim', true);
                    this.coinSound.play();
                    this.addFlyingPoints(block.x, block.y - 16, block.p);

                    this.tweens.add({
                        targets: coin,
                        y: { from: coin.y, to: coin.y - 48 },
                        ease: 'Cubic',
                        duration: 500,
                        yoyo: true,
                        onComplete: () => {
                            coin.destroy();
                        },
                    });                
                    break;    
                case 'red-m':
                case 'green-m':
                    let mushroom = this.prizes.create(block.x - 8, block.y, block.t).setDepth(0).setOrigin(0).refreshBody();
                    mushroom.setImmovable(true);
                    mushroom.body.allowGravity = false;

                    this.mushroomSound.play();
                    this.addFlyingPoints(block.x, block.y - 16, block.p);

                    this.tweens.add({
                        targets: mushroom,
                        y: { from: mushroom.y - 8, to: mushroom.y - 16 },
                        ease: 'Linear',
                        duration: 1000,
                        onComplete: () => {
                            mushroom.setVelocityX(60);
                            mushroom.setImmovable(false);
                            mushroom.body.allowGravity = true;
                        },
                    });                
    
                    break;    
                }

                block.immovable = true;
                block.anims.stop();
                block.setFrame(3);
                this.tweens.add({
                    targets: block,
                    y: { from: block.y, to: block.y - 8 },
                    ease: 'Cubic',
                    duration: 100,
                    repeat: 0,
                    yoyo: true
                });
            }
        });

        this.jumpSmall = this.sound.add('jump-small', {
            mute: false, // mute: true/false
            volume: 1, // volume: 0 to 1
            rate: 1, // rate: 1.0(normal speed), 0.5(half speed), 2.0(double speed)
            detune: 0, // detune: -1200 to 1200
            seek: 0, // seek: playback time
            loop: false,
            delay: 0
        });

        this.jumpBig = this.sound.add('jump-big', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.powerUp = this.sound.add('power-up', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.coinSound = this.sound.add('coin-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.mushroomSound = this.sound.add('mushroom-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.stompSound = this.sound.add('stomp-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.dieSound = this.sound.add('die-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.flagPole = this.sound.add('flag-pole', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.stageClear = this.sound.add('stage-clear', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.music = this.sound.add('music', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: true, delay: 0 });
        this.music.play();
    }

    update() {
        if (this.levelEnds) {            
            return;
        }

        if (this.mario.y > this.background.height - this.mario.height) { // Checkea si Mario cae opr debajo del ground (o sea en un pozo)
            this.mario.die();
        } else if (this.mario.x > this.finalFlag.x) { // Checkea si Mario pasa la bandera final
            this.input.keyboard.enabled = false;
            // Posiciona a Mario por encima de la bandera
            if (!this.mario.isSliding()) {
                if (this.mario.y < this.finalFlag.y) {
                    this.mario.y = this.finalFlag.y - this.mario.height;
                }
                this.mario.slideDown(this.finalFlag.x + 8);
                this.flagPole.play();
                this.timer.remove();
            }

            this.mario.disableBody(false, false);
            
            if ((this.mario.y > this.finalFlag.y && this.mario.y < 176) || (this.mario.y <= this.finalFlag.y && this.finalFlag.y < 168)) {
                this.mario.y += 2;
                this.finalFlag.y += 2;
            } else {
                this.music.stop();
                this.stageClear.play();
                this.mario.goCastleDoor();
                this.levelEnds = true;
            }        

        } else if (this.mario.isAlive()) { // Keyboard controls
            let camera = this.cameras.main;

            if (this.cursors.left.isDown) { // Izquierda
                this.mario.turnLeft();
                if (this.mario.x > camera.scrollX) {
                    this.mario.run();
                } else {
                    this.mario.stop(camera.scrollX);
                }
            } else if (this.cursors.right.isDown) { // Derecha
                this.mario.turnRight();
                if (this.mario.x < camera.scrollX + CAMERA_MOTION_POINT || camera.scrollX >= this.background.width - SCENE_WIDTH) {
                    if (this.mario.x < this.background.width - this.mario.width) {
                        this.mario.run();
                    } else {
                        this.mario.stop(this.background.width - this.mario.width);
                    }
                } else {
                    if (camera.scrollX < this.background.width - SCENE_WIDTH) {
                        camera.scrollX += CAMERA_VELOCITY; // Mueve la cámara
                    }
                }
            } else { 
                this.mario.stand();
            }

            if ((this.cursors.space.isDown || this.cursors.up.isDown) && this.mario.isOnGround()) {
                this.mario.jump();
            }
            
            // Movimientos de los enemigos
            this.moveEnemies();
        } else {
            this.mario.dying();
        }
    }

    moveEnemies() {
        for (const enemy of this.enemies.getChildren()) {
            // Revisa si el enemigo está en el viewport de la cámara
            if (this.cameras.main.worldView.contains(enemy.x, enemy.y)) {
                if (enemy.body.velocity.x == 0) {
                    enemy.setVelocityX(ENEMY_VELOCITY);
                }
            }
        }
    }

    stopEnemies() {
        this.enemies.setVelocityX(0);
        for (const enemy of this.enemies.getChildren()) {
            enemy.anims.stop();
        }
    }

    setTime() {
        let newTime = (`00${this.counter}`).slice(-3);
        this.timeText.setText(newTime);
    }


    addPoints(points) {
        this.score += points;
        let scoreFilled = (`000000${this.score}`).slice(-6);
        this.scoreText.setText(scoreFilled);
    }

    addFlyingPoints(x, y, points) {
        this.addPoints(parseInt(points));
        let flyPoints = this.add.image(x, y - 16, points).setDepth(1).setOrigin(0);
        this.tweens.add({
            targets: flyPoints,
            x: '+=32',
            y: '-=32',
            ease: 'Linear',
            duration: 2500,
            onComplete: () => {
                flyPoints.destroy();     
            },
        });
    }
    
    endLevel() {
        this.time.addEvent({
            delay: 20,
            repeat: this.counter,
            callback: () => {
                this.setTime();
                this.addPoints(100);
                this.counter--;
                if (this.counter == 0) {
                    // Aparece la bandera del castillo
                    this.tweens.add({
                        targets: this.castleFlag,
                        y: '-=16',
                        ease: 'Linear',
                        duration: 800,
                        yoyo: false,
                        onComplete: () => {
                            this.restartGame();
                        }
                    });
                }
            }
        });

    }

    restartGame() {
        // Reinicia la escena después de 3 segundos con la última puntuación
        this.time.addEvent({
            delay: END_GAME_DELAY,
            callback: () => {
                this.scene.restart( { score: this.score } );
            }
        });
    }

}

/**
 * Mario class
 */
const MARIO_INIT_X = 40;
const MARIO_INIT_Y = 192;
const MARIO_VELOCITY = 100;
const MARIO_JUMP_VELOCITY = 200;

class Mario extends Phaser.Physics.Arcade.Sprite {
    constructor(scene) {
        super(scene);
        this.scene = scene;

        this.lives = 3; // TODO
        this.alive = true;
        this.slide = false;
        this.protected = false;
        this.size = 'small';
        this.setTexture(`mario-${this.size}`);
        this.setDepth(1);
        this.setOrigin(0);
        this.setPosition(MARIO_INIT_X, MARIO_INIT_Y);

        this.scene.add.existing(this); // Add the sprite to the scene
        this.scene.physics.add.existing(this); // Add the sprite body to the scene
    }

    isAlive() {
        return this.alive;
    }

    fire() { // TODO
    }

    stand() {
        this.setVelocityX(0); // Sets the horizontal component of the body's velocity to zero
        this.anims.stop();
        if (this.isOnGround()) {
            this.standing();
        } else {
            this.jumping();
        }
    }

    standing() {
        this.setFrame(0);
    }

    run() {
        let velocity = this.flipX ? MARIO_VELOCITY * -1 : MARIO_VELOCITY;
        this.setVelocityX(velocity);
    }

    running() {
        this.anims.play(`mario-${this.size}-runs-anim`, true); // Run animation
    }

    jump() {
        this.setVelocityY(MARIO_JUMP_VELOCITY * -1);
        this.scene.jumpSmall.play(); // Play jump sound
    }

    jumping() {
        this.setFrame(5);
    }

    turnLeft() {
        this.flipX = true;
        if (this.isOnGround()) {
            this.running();
        } else {
            this.jumping();
        }
    }

    turnRight() {
        this.flipX = false;
        if (this.isOnGround()) {
            this.running();
        } else {
            this.jumping();
        }
    }

    stop(x) {
        this.setX(x);
    }

    isOnGround() {
        return this.body.touching.down;
    }

    isProtected() {
        return this.protected;
    }

    hurt() {
        if (!this.isProtected()) {
            if (this.isBig()) {
                this.shrink();
            } else {
                this.die()
            }
        }
    }

    die() {
        if (this.isAlive()) {
            this.scene.music.stop();
            this.alive = false;
            this.scene.tweens.add({
                targets: this,
                y: '-=48',
                ease: 'Cubic',
                duration: 400,
                yoyo: true,
                onStart: () => {                        
                    this.scene.dieSound.play();
                    this.scene.stopEnemies();
                },
                onComplete: () => {
                    this.setImmovable(true);
                    this.scene.restartGame();
                }
            });
        }
    }

    isBig() {
        return this.size == 'big';
    }
    
    dying() {
        this.setVelocityX(0);
        this.anims.stop();
        this.frizzing();
    }

    frizzing() {
        this.setFrame(13);
    }

    sliding() {
        this.anims.play(`mario-${this.size}-slide-down`, true); // Run animation
    }

    slideDown(x) {
        this.slide = true;
        this.setX(x);
        this.sliding();
    }

    isSliding() {
        return this.slide;
    }

    goCastleDoor() {
        this.anims.stop();
        this.x += 16;
        this.flipX = true;

        // Jump to groung
        this.scene.tweens.add({
            targets: this,
            x: '+=16',
            y: { from: this.y, to: 208 - this.height },
            ease: 'Linear',
            duration: 400,
            yoyo: false,
            onStart: () => {                        
                this.jumping();
            },
            onComplete: () => {
                this.flipX = false;
                // Run to the door
                this.scene.tweens.add({
                    targets: this,
                    x: '+=72',
                    ease: 'Linear',
                    duration: 600,
                    yoyo: false,
                    onStart: () => {                        
                        this.running();
                    },
                    onComplete: () => {
                        // Enter in the castle
                        this.scene.tweens.add({
                            targets: this,
                            alpha: { from: 1, to: 0 },
                            ease: 'Linear',
                            duration: 400,
                            yoyo: false,
                            onComplete: () => {
                                this.scene.endLevel();                                
                            }
                        });
        
                    }
                });
            }
        });
    }

    grow() {
        let x = this.x, y = this.y;
        this.size = 'big';
        this.setVisible(false);
        this.scene.powerUp.play();
        let grow = this.scene.add.sprite(x, y - 16, 'mario-sizes').setDepth(1).setOrigin(0);
        grow.anims.play('mario-grows-anim', true);
        grow.on(Phaser.Animations.Events.ANIMATION_COMPLETE, () => {
            this.setTexture(`mario-${this.size}`);
            this.setPosition(x, y - 16);
            this.body.setSize(this.width, this.height, false);
            this.refreshBody();
            this.setVisible(true);
            grow.destroy();
        }, this.scene);
    }

    shrink() {
        let x = this.x, y = this.y;
        // Protect Mario for 3 seconds
        this.protected = true;
        this.scene.tweens.add({
            targets: this,
            alpha: { from: 1, to: 0 },
            ease: 'Linear',
            duration: 50,
            repeat: 60,
            yoyo: true,
            onComplete: () => {
                this.protected = false;
            }
        });
                
        this.size = 'small';
        this.setVisible(false);
        this.scene.powerUp.play();
        let shrink = this.scene.add.sprite(x, y, 'mario-sizes').setDepth(1).setOrigin(0);
        shrink.anims.play('mario-shrinks-anim', true);
        shrink.on(Phaser.Animations.Events.ANIMATION_COMPLETE, () => {
            this.setTexture(`mario-${this.size}`);
            this.setPosition(x, y + 16);
            this.body.setSize(this.width, this.height, false);
            this.refreshBody();
            this.setVisible(true);
            shrink.destroy();
        }, this.scene);

    }

    hurtEnemy(enemy) {
        enemy.die = true;

        this.scene.tweens.add({
            targets: this,
            y: '-=8',
            ease: 'Linear',
            duration: 200,
            yoyo: false,
            onStart: () => {
                this.scene.stompSound.play();
                this.scene.addFlyingPoints(enemy.x, enemy.y - 16, '100');
                enemy.setVelocity(0);
                enemy.anims.stop();
                enemy.setFrame(2);
            },
            onComplete: () => {
                enemy.destroy();
            }
        });
    }

}

var config = {
    type: Phaser.AUTO,
    width: SCENE_WIDTH,
    height: SCENE_HEIGHT,
    antialias: false,
    pixelArt: true,
    roundPixels: true,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH, 
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
    },
    scene: Game
};

var game = new Phaser.Game(config);